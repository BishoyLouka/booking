
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: "Book Liturgy",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Muli:400,300' },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { 
    color: '#F3BB45' ,
    continuous : true,
    height : "15px"
  },
  /*
  ** Global CSS
  */
  css: [
    "@/assets/css/bootstrap.min.css",
    "@/assets/css/themify-icons.css",
    "@/assets/scss/theme.scss"
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    "~/plugins/vue-date-pick.js",
    "~/plugins/vuelidate.js",
    "~/plugins/firebase.js"
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    'vue-scrollto/nuxt'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
