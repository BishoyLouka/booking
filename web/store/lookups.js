import { each } from 'lodash'
import moment from 'moment'

export const state = () => ({
  schedule: []
})

export const mutations = {
  changeSchedule(state, schedule) {
    state.schedule = schedule
  }
}

export const actions = {
  changeSchedule({ commit }, schedule) {
    each(schedule, itm => {
      debugger;
      itm.date = moment(itm.date.toDate())
    })
    schedule = _.orderBy(schedule, 'date');
    commit('changeSchedule', schedule)
  }
}
