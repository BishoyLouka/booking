import { initializeApp } from 'firebase/app'
import 'firebase/firestore';
import "firebase/auth";

fetch('/__/firebase/init.json').then(async res => {
  const config = await res.json()
  initializeApp(config);
  // if (process.env.NODE_ENV === 'development')
  //   firestore().settings({
  //     host: 'localhost:8080',
  //     ssl: false
  //   })
})
