import { map } from "lodash";

import { admin, Message } from "../service-bus";
import * as processManager from "./processes";
import { StepStatus, AttributeSteps } from "./process-descriptor";

export default async function startProcess(
  name: string,
  params: { [key: string]: any }
) {
  const process = processManager.get(name);

  const stepsAttribute: AttributeSteps[] = map(process.steps, (step) => ({
    topic: step.topicName || step.name,
    name: step.name,
    status: StepStatus.NotStarted,
  }));

  const message: Message = {
    Label: "process",
    attributes: {
      process: name,
      steps: stepsAttribute,
    },
    body: params,
  };

  const firstTopic = await admin
    .serviceBus()
    .topic(process.steps[0].topicName || process.steps[0].name);

  await firstTopic.publishMessage(message);
}
