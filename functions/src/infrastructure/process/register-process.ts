import { CloudFunction } from "firebase-functions";
import { QueryDocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import { each, extend } from "lodash";

import { admin, functions, Message } from "../service-bus";
import {
  ProcessDescriptor,
  AttributeSteps,
  StepStatus,
} from "./process-descriptor";
import * as processManager from "./processes";

export default function registerProcess(
  process: ProcessDescriptor
): { [name: string]: CloudFunction<QueryDocumentSnapshot> } {
  processManager.set(process);
  const serviceBus = admin.serviceBus();
  const exportedListners: {
    [name: string]: CloudFunction<QueryDocumentSnapshot>;
  } = {};

  processManager.set(process);

  const listenerFnc = (ndx: number, handler: Function) => {
    return async (msg: Message) => {
      const input = msg.body;
      const steps = msg.attributes?.steps as AttributeSteps[];

      steps[ndx].status = StepStatus.InProgress;
      let output = handler.apply(msg, [input]);
      msg.body = extend(await Promise.resolve(output), input);
      steps[ndx].status = StepStatus.Done;
      if (ndx + 1 < steps.length) {
        let nextTopic = await serviceBus.topic(steps[ndx + 1].topic);
        await nextTopic.publishMessage(msg);
      }
    };
  };

  each(process.steps, (step, ndx) => {
    exportedListners[process.name + "_" + step.name] = functions
      .topic(step.topicName || step.name)
      .onMessage(listenerFnc(ndx, step.function));
  });
  return exportedListners;
}
