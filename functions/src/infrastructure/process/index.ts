import { ProcessDescriptor } from "./process-descriptor";
import register from "./register-process";
import start from "./start-process";

export { register, start, ProcessDescriptor };
