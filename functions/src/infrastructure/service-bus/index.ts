import * as admin from "./admin.extension";
import * as functions from "./function-extension";
import { Message } from "./message";

export { admin, functions, Message };
