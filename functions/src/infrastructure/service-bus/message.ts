export interface Message {
    body: any;
    id?: string;
    timestamp?: Date;
    Label?: string;
    attributes?: { [key: string]: any };
    lock?: string;
  }