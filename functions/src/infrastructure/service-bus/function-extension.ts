import { CloudFunction, firestore } from "firebase-functions";
import { QueryDocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import { firestore as adminFirestore } from "firebase-admin";
import { extend } from "lodash";

import { Message } from "./message";

export function topic(name: string): TopicBuilder {
  return new TopicBuilder(name);
}

export interface OnMessageOptions {
  readMode: "receiveAndDelete" | "pickLock";
}
export interface MessageContext {
  auth?: {
    token: object;
    uid: string;
  }
  eventId: string;
  timestamp: string;
  readMode?: "receiveAndDelete" | "pickLock";
  deleteMessage: () => Promise<void>;
  unLockMessage: () => Promise<void>;
}

export class TopicBuilder {
  constructor(private name: string) {}

  onMessage(
    handler: (message: Message, context: MessageContext) => PromiseLike<any> | any,
    options?: OnMessageOptions
  ): CloudFunction<QueryDocumentSnapshot> {
    options = extend({ readMode: "receiveAndDelete" }, options);
    return firestore
      .document(
        `${process.env.C_SERVICEBUSS_COLL}/${process.env.C_TOPIC_DOC}/${this.name}/{message}`
      )
      .onCreate(async (doc, ctx) => {
        //FIXME: remove the firebaseAdmin from here ....
        const sbContext = {
          auth: ctx.auth,
          eventId: ctx.eventId,
          timestamp: ctx.timestamp,
          readMode: options?.readMode,
          deleteMessage: async () => {
            await doc.ref.delete();
          },
          unLockMessage: async () => {
            await doc.ref.update({
              lock: adminFirestore.FieldValue.delete(),
            });
          },
        };
        const output = await handler(doc.data() as Message, sbContext);
        if (options?.readMode == "receiveAndDelete") {
          doc.ref.delete();
        } else if (options?.readMode == "pickLock") {
          await doc.ref.set({ lock: new Date().getTime().toString() });
        }
        return output;
      });
  }
}
