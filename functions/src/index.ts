import { https } from "firebase-functions";
import { initializeApp, firestore } from "firebase-admin";

initializeApp();

export { registrationListeners } from "./process/registration";
export { onNewRequest } from "./listeners/new-request";

// IMPORTS
// export const importShedule = https.onRequest(onRequest_importSheduleFunction);

export const testProcess = https.onRequest(async (req, res) => {
  await firestore()
    .collection("requests")
    .add({
      schedule: {
        location: "St Paul - Tensta",
        date: "2021-02-23T11:04:46.620Z",
        id: "8pR5HPLwv5Wk4CH",
      },
      firstName: "Mina",
      lastName: "Alphonce",
      numberOfAttendees: 10,
      email: "ya@rab.com",
      mobile: "0011223344"
    });
  res.sendStatus(200);
});
