import nodemailer from "nodemailer";
import { firestore } from "firebase-admin";
import { template } from "lodash";

import emailTemplate from "./email-template.html";
const compiledEmail = template(emailTemplate);

const transport = nodemailer.createTransport({
  host: process.env.C_SMTP_HOST,
  port: Number.parseInt(process.env.C_SMTP_PORT || "25"),
  auth: {
    user: process.env.C_SMTP_AUTH_USER,
    pass: process.env.C_SMTP_AUTH_PASS,
  },
});

export default async function sendMail(params: { path: string }) {
  const ref = await firestore().doc(params.path).get();
  const docData = ref.data();
  await transport.sendMail({
    from: "no-reply@philos-platform.com",
    to: docData?.email || "default@philos-platform.com",
    subject: "Registration completed",
    html: compiledEmail({ id: ref.id, ...docData }),
  });
}
