import { register } from "../../infrastructure/process";

import generateQRFunction from "./generate-qr";
import sendEmailFUnction from "./send-mail";

export const registrationListeners = register({
  name: "registration",
  steps: [
    {
      name: "GenerateQR",
      function: generateQRFunction,
    },
    {
      name: "SendEmail",
      function: sendEmailFUnction,
    },
  ],
});
