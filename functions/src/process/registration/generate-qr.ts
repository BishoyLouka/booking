import { firestore } from "firebase-admin";
import QRCode from "qrcode";

export default async function generateQR(params: { path: string }) {
  const doc = await firestore().doc(params.path).get();
  const qrCodeURL = await QRCode.toDataURL(JSON.stringify(doc.data(), null, 2));
  doc.ref.update({ qrCode: qrCodeURL });
}
