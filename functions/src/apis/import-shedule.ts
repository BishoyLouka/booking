import * as admin from "firebase-admin";
import { Request, Response } from "firebase-functions";

export default async function (req: Request, res: Response) {
  if (req.method == "POST") {
    var collection = admin.firestore().collection("schedule");

    for (var item of req.body.schedule) {
      collection.add({
        location: item.location,
        date: new Date(item.date),
        count: item.count,
        countLeft: 0,
      });
    }
    res.sendStatus(200);
  } else {
    res.sendStatus(404);
  }
}
