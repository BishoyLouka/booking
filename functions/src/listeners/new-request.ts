import { firestore } from "firebase-functions";

import { start } from "../infrastructure/process";

export const onNewRequest = firestore.document("/requests/{requestId}").onCreate(async doc => {
    return await start("registration",{
        code : doc.id,
        path : doc.ref.path
    })
});